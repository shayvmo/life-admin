<?php


namespace App\Services\Base;

use App\Models\Config;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfigService
{
    const JOIN_STR = '_';
    const SAVE_JOIN_STR = '.';

    public function getAllConfig(): array
    {
        $field = [
            'id',
            'group',
            'only_sign',
            'name',
            'value',
            'field_type',
            'option_value',
            'remark',
        ];

        return Cache::remember($this->getConfigCacheKey(), now()->addDay(), function () use ($field) {
            $data = Config::query()->select($field)->orderBy('group', 'asc')->get()->toArray();
            $return_data = [];
            foreach ($data as $item) {
                $key = str_replace(self::SAVE_JOIN_STR, self::JOIN_STR, $item['only_sign']);
                $return_data[$key] = $item;
            }
            return $return_data;
        });
    }

    public function saveConfig(array $data): bool
    {
        $cache_data = $this->getConfigCache();
        $update_data = [];
        foreach ($data as $key => $item) {
            if (isset($cache_data[$key]) && $cache_data[$key]['value'] != $item['value']) {
                $update_data[$item['only_sign']] = $item['value'] ?? '';
            }
        }
        if (!empty($update_data)) {
            DB::transaction(function () use ($update_data) {
                foreach ($update_data as $key => $item) {
                    $config = Config::query()->whereOnlySign($key)->firstOrFail();
                    $config->fill(['value' => $item])->saveOrFail();
                }
            });
            Cache::forget($this->getConfigCacheKey());
            return true;
        }
        return false;
    }

    /**
     * @Description 同步自定义的配置
     * @Author shayvmo
     * @Date 2021/1/11 17:10
     */
    public function sync()
    {
        Log::warning('同步配置');
//        if (!Request::is('install') && !Request::is('logs')) {
//            $config = $this->getAllConfig();
//            foreach ($config as $item) {
//                config([$item['only_sign'] => $item['value']]);
//            }
//        }

    }

    private function getConfigCache()
    {
        return Cache::get($this->getConfigCacheKey(), []);
    }

    private function getConfigCacheKey()
    {
        return 'config:cache';
    }
}
