<?php


namespace App\Services\Base;


use App\Exceptions\ValidateException;
use App\Models\Admin;
use App\Models\RequestLog;
use Illuminate\Support\Facades\Validator;

class CommonService
{
    /**
     * 校验
     * @param array $params
     * @param array $rules
     * @return bool
     * @throws ValidateException
     */
    public static function validate(array $params, array $rules): bool
    {
        $rules_demo = [
            'username' => [
                'required',
                'alpha_num',
                'chinese',
                'between:5,18',
                'unique:admins,username',
            ],
        ];
        $validator = Validator::make($params, $rules);
        if ($validator->fails()) {
            throw new ValidateException($validator->errors()->first());
        }
        return true;
    }

    /**
     * 校验用户是否有权限
     * @param int $user_id
     * @param string $controller_action
     * @return bool
     */
    public static function checkUserAuth(int $user_id, string $controller_action): bool
    {
        $admin = Admin::query()->with([
            'role' => function ($query) {
                $query->with([
                    'rules' => function ($query) {
                        $query->select(['value']);
                    }
                ])->select(['id','name']);
            }
        ])->select([
            'id',
            'role_id',
        ])->findOrFail($user_id);
        if (empty($admin->role)) {
            return false;
        }
        $auth_list = [];
        $role_auth_rules = $admin->role->rules;
        if(!empty($role_auth_rules)) {
            foreach ($role_auth_rules as $rule) {
                $auth_list[] = $rule->value;
            }
        }
        if(in_array($controller_action,$auth_list)) {
            return true;
        }
        return false;
    }

    /**
     * 获取当前请求节点（用于校验jwt）
     * @return mixed|string|string[]
     */
    public static function getRequestEndpoint()
    {
        $namespace = request()->route()->getAction('namespace');
        $namespace = str_replace('App\Http\Controllers\\', '', $namespace);
        $namespace = str_replace('\\', '-', $namespace);
        return $namespace;
    }

    /**
     * @Description 转换分页数据到指定格式
     * @Author shayvmo
     * @Date 2020/12/1 12:28
     * @param array $page_data
     * @return array
     */
    public static function changePageDataFormat(array $page_data)
    {
        return [
            "current_page" => $page_data['current_page'],
            "per_page" => $page_data['per_page'],
            "total" => $page_data['total'],
            "data" => $page_data['data'],
        ];
    }

    /**
     * 请求日志
     * @param array $params
     */
    public static function addRequestLog(array $params = []): void
    {
        RequestLog::query()->create([
            'admin_id' => $params['admin_id'],
            'type_id' => $params['type_id'],
            'ip_address' => $params['ip_address'],
            'api_name' => $params['api_name'],
            'request_method' => $params['request_method'],
            'request_header' => $params['request_header'],
            'request_get' => $params['request_get'],
            'request_post' => $params['request_post'],
            'response_content' => $params['response_content'],
            'remark' => $params['remark'],
        ]);
    }
}
