<?php

namespace App\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $file = app()->make(Filesystem::class);
        $dirs = $file->directories(base_path('addons'));
        foreach ($dirs as $dir) {
            $sign = $file->name($dir);
            app()->register("\\Addons\\{$sign}\\ServiceProvider");
        }
    }
}
