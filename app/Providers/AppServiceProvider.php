<?php

namespace App\Providers;

use App\Http\Validators\ChineseTextValidator;
use App\Http\Validators\HashValidator;
use App\Http\Validators\IdNumberValidator;
use App\Http\Validators\PhoneValidator;
use App\Http\Validators\UsernameValidator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    protected $validators = [
        'phone' => PhoneValidator::class,
        'id_no' => IdNumberValidator::class,
        'hash' => HashValidator::class,
        'username' => UsernameValidator::class,
        'chinese' => ChineseTextValidator::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // 注册验证器
        $this->registerValidators();
        // 打印sql log
        $this->dumpSqlLog();

        if (!$this->app->runningInConsole()) {
            // 自定义配置
//            $this->app->make(ConfigServiceInterface::class)->sync();
        }
    }

    /**
     * Register validators.
     */
    protected function registerValidators()
    {
        foreach ($this->validators as $rule => $validator) {
            Validator::extend($rule, "{$validator}@validate");
        }
    }

    /**
     * sql log
     */
    public function dumpSqlLog()
    {
        DB::listen(
            function ($sql) {
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else {
                        if (is_string($binding)) {
                            $sql->bindings[$i] = "'$binding'";
                        }
                    }
                }

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

                $query = vsprintf($query, $sql->bindings);

                // Save the query to file
                $logFile = fopen(
                    storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d-H') . '-query.log'),
                    'a+'
                );
                fwrite($logFile, date('Y-m-d H:i:s') . ': ' . $query . PHP_EOL . PHP_EOL);
                fclose($logFile);
            }
        );
    }
}
