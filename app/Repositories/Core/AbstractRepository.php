<?php
/**
 * eshop
 *
 * @ClassName AbsrtractRepository
 * @Author Eric
 * @Date 2021-07-31 10:28 星期六
 * @Version 1.0
 * @Description
 */


namespace App\Repositories\Core;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

abstract class AbstractRepository implements RepositoryInterface
{
    private $app;

    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }
    }
}
