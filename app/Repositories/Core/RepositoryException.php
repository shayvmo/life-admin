<?php
/**
 * eshop
 *
 * @ClassName RepositoryException
 * @Author Eric
 * @Date 2021-07-31 10:33 星期六
 * @Version 1.0
 * @Description
 */


namespace App\Repositories\Core;


class RepositoryException extends \Exception
{

}
