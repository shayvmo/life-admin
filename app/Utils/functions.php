<?php
/**
 * Author: shayvmo
 * CreateTime: 2021/06/22 12:13:57
 * Description: 助手函数
 */

if (!function_exists('check_chinese_text')) {
    /**
     * 校验是否包含文字
     * @param string $string
     * @param bool $filter 是否过滤，是则返回过滤后的字符串
     * @return bool|string|string[]|null
     */
    function check_chinese_text(string $string,$filter = false)
    {
        $pattern = '/[\\x80-\\xff]/';
        if(preg_match($pattern,$string)) {
            if($filter) {
                return preg_replace($pattern,'',$string);
            }
            return true;
        }
        return false;
    }
}

if (!function_exists('get_data_tree')) {
    /**
     * 生成树形数组
     * @param $arr
     * @param string $auto_id_name
     * @param string $parent_id_name
     * @param string $children_name
     * @return array
     */
    function get_data_tree($arr, $auto_id_name = 'id', $parent_id_name = 'pid', $children_name = 'children'): array
    {
        $refer = array();
        $tree = array();
        foreach($arr as $k => $v){
            $refer[$v[$auto_id_name]] = & $arr[$k]; //创建主键的数组引用
        }
        foreach ($arr as $k => $v) {
            $pid = $v[$parent_id_name];  //获取当前分类的父级id
            if ($pid == 0) {
                $tree[] = &$arr[$k];  //顶级栏目
            } else {
                if (isset($refer[$pid])) {
                    $refer[$pid][$children_name][] = &$arr[$k]; //如果存在父级栏目，则添加进父级栏目的子栏目数组中
                }
            }
        }
        return $tree;
    }
}

if (!function_exists('check_mobile')) {
    /**
     * 校验是否为有效手机号
     * @param $string
     * @return bool
     */
    function check_mobile($string): bool
    {
        return preg_match('/^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[0135678]|8[0-9]|99)\d{8}$/', $string);
    }
}



if (!function_exists('get_mock_mobile')) {
    /**
     * 获取模拟的手机号
     * @return string
     * @throws Exception
     */
    function get_mock_mobile(): string
    {
        return '1'.random_int(30,39).random_int(1000,9999).random_int(1000,9999);
    }
}

if (!function_exists('generate_code')) {
    /**
     * 生成随机码
     * @param int $length 长度
     * @param array $type 码包含类型，number 数字 special 特殊字符 lower 小写 upper 大写
     * @return string
     */
    function generate_code(int $length = 16,array $type = ['number','special','lower','upper']): string
    {
        $source_string = [
            'lower' => str_shuffle('abcdefghijklmnopqrstuvwxyz'),
            'upper' => str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
            'number' => str_shuffle('0123456789'),
            'special' => str_shuffle('!@#$%^&*'),
        ];

        if(empty($type)) {
            return '';
        }

        $parts = ceil($length/count($type) );

        $temp = array_map(function($item) use ($parts, $source_string) {
            if(isset($source_string[$item])) {
                $temp_string = '';
                do{
                    $temp_string .= substr(str_shuffle($source_string[$item]),0,$parts);
                }while(strlen($temp_string) < $parts);
                return substr($temp_string,0,$parts);
            }
            return substr(str_shuffle(
                $source_string['lower']
                .$source_string['number']
                .$source_string['upper']
                .$source_string['special']),0,$parts);
        }, $type);

        return str_shuffle(substr(implode('',$temp), 0, $length));
    }
}

if (!function_exists('get_date_moments_format')) {
    /**
     * @Description 获取类似微信朋友圈时间格式
     * @Author shayvmo
     * @Date 2021/5/13 14:58
     * @param int|string $create_time 待转换时间字符串 Y-m-d H:i:s 或者是 时间戳int
     * @return false|string
     */
    function get_date_moments_format($create_time) {
        if (is_string($create_time)) {
            $create_time = strtotime($create_time);
        }
        if ($create_time === false) {
            return false;
        }
        $now_time = time();
        if (date ( "Y", $now_time ) === date ( "Y", $create_time )) {
            $time = $now_time - $create_time;
            if ($time < 60) {
                return "刚刚";
            }

            $sec = $time / 60;
            if ($sec < 60) {
                return round ( $sec ) . "分钟前";
            }

            $hours = $time / 3600;
            if ($hours < 48) {

                if (date ( 'Ymd', $create_time ) + 1 === (int)date ( 'Ymd', $now_time )) {
                    return "昨天 " . date ( "H:i", $create_time );
                } elseif ($hours < 24) {
                    return round ( $hours ) . "小时前";
                }
            }

            return date ( "m月d日 H:i", $create_time );
        } else {
            return date ( "Y年m月d日 H:i", $create_time );
        }
    }
}

if (!function_exists('get_distance')) {
    /**
     * @Description 求两个已知经纬度之间的距离,单位为米
     * @Author shayvmo
     * @Date 2021/6/22 12:08
     * @param float $lng1 位置1经度
     * @param float $lat1 位置1纬度
     * @param float $lng2 位置2经度
     * @param float $lat2 位置2纬度
     * @return float 距离，单位米
     */
    function get_distance(float $lng1,float $lat1,float $lng2,float $lat2): float
    {
        // 将角度转为狐度
        $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        return 2 * asin(sqrt(sin($a / 2) ** 2 + cos($radLat1) * cos($radLat2) * (sin($b / 2) ** 2))) * 6378.137 * 1000;
    }
}

if (!function_exists('mysql_timestamp')) {
    /**
     * 生成mysql数据库时间戳（eg. 2000-01-01 12:00:00）
     * @param integer $time
     * @return false|string
     */
    function mysql_timestamp($time = null)
    {
        return date('Y-m-d H:i:s', $time ?: time());
    }
}














