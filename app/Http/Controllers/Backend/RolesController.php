<?php

namespace App\Http\Controllers\Backend;

use App\Constants\SystemConstant;
use App\Exceptions\ServiceException;
use App\Exceptions\ValidateException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\RoleListRequest;
use App\Http\Requests\Backend\RolePostRequest;
use App\Http\Requests\Backend\RolePermissionsPostRequest;
use App\Models\Role;
use App\Services\Base\CommonService;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Permission;

/**
 * Class RolesController
 * @package App\Http\Controllers\Backend
 * @signName 角色管理
 * @sign role
 */
class RolesController extends Controller
{
    /**
     * @Description 角色列表
     * @Author shayvmo
     * @Date 2021/4/15 10:53 下午
     * @param RoleListRequest $roleListRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(RoleListRequest $roleListRequest)
    {
        [
            'page_size' => $page_size,
            'title' => $title,
            'type' => $type,
        ] = $roleListRequest->fillData();
        $data = Role::query()
            ->when($title, function ($query) use ($title) {
                return $query->where('title', 'like', "%$title%");
            })->when($type === 'all', function($query) {
                return $query->get();
            }, function($query) use ($page_size) {
                return $query->paginate($page_size);
            })->toArray();

        return $this->successData($type === 'all' ? $data : CommonService::changePageDataFormat($data));
    }

    /**
     * @Description 新增角色
     * @Author shayvmo
     * @Date 2021/4/15 10:53 下午
     * @param RolePostRequest $rolePostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidateException
     */
    public function store(RolePostRequest $rolePostRequest)
    {
        $params = $rolePostRequest->fillData();
        $rules = [
            'name' => [
                'unique:roles,name',
            ],
        ];
        CommonService::validate($params, $rules);
        $params['desc'] = $params['desc'] ?? '';
        $role = Role::query()->create($params);
        return $this->successData($role->toArray());
    }

    /**
     * @Description 角色详情
     * @Author shayvmo
     * @Date 2021/4/15 10:53 下午
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Role $role)
    {
        return $this->successData($role->toArray());
    }

    /**
     * @Description 编辑角色
     * @Author shayvmo
     * @Date 2021/4/15 10:53 下午
     * @param Role $role
     * @param RolePostRequest $rolePostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ServiceException | ValidateException
     */
    public function update(Role $role,RolePostRequest $rolePostRequest)
    {
        $params = $rolePostRequest->fillData();
        if ($role->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        if ($role->name != $params['name']) {
            $rules = [
                'name' => [
                    'unique:roles,name',
                ],
            ];
            CommonService::validate($params, $rules);
        }
        $params['desc'] = $params['desc'] ?? '';
        $role->fill($params)->save();
        return $this->successData($role->toArray());
    }

    /**
     * @Description 删除角色
     * @Author shayvmo
     * @Date 2021/4/15 10:54 下午
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ServiceException
     */
    public function destroy(Role $role)
    {
        if($role->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        try {
            $role->delete();
        } catch (QueryException $queryException) {
            throw new ServiceException(SystemConstant::DELETE_FAILED);
        }

        return $this->success();
    }

    /**
     * @Description 获取角色权限
     * @Author shayvmo
     * @Date 2021/4/15 10:54 下午
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRolePermissions(Role $role)
    {
        $all_permissions = Permission::all();
        $role_permissions = $role->getAllPermissions();
        $lists = get_data_tree($all_permissions->toArray());
        $checked_key_ids = $role_permissions
            ->pluck('id')
            ->diff(
                $all_permissions
                    ->countBy('pid')
                    ->diffAssoc($role_permissions->filter(function($item) {
                            return $item->pid > 0;
                        })->countBy('pid')
                    )->keys()
            )->values()
            ->all();// 过滤没有全选的父节点后选中的节点ID
        $checked_key_names = $role_permissions->filter(function($item) use ($checked_key_ids) {
            return in_array($item->id, $checked_key_ids, true);
        })->pluck('name')->all();// 选中的节点对应的name
        return $this->successData([
            'lists'=>$lists,
            'checkedKeys'=>$checked_key_names,
        ]);
    }

    /**
     * @Description 设置角色权限
     * @Author shayvmo
     * @Date 2021/4/15 10:54 下午
     * @param Role $role
     * @param RolePermissionsPostRequest $rolePermissionsPostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function setRolePermissions(Role $role,RolePermissionsPostRequest $rolePermissionsPostRequest)
    {
        [
            'rules' => $rules
        ] = $rolePermissionsPostRequest->fillData();
        if ($role->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        $role->syncPermissions($rules);
        return $this->success();
    }
}
