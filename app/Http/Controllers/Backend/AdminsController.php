<?php

namespace App\Http\Controllers\Backend;

use App\Constants\BackendConstant;
use App\Constants\SystemConstant;
use App\Exceptions\ServiceException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AdminListRequest;
use App\Http\Requests\Backend\AdminPostRequest;
use App\Models\Admin;
use App\Services\Base\CommonService;

/**
 * 管理员
 * Class AdminsController
 * @package App\Http\Controllers\Backend\System
 * @signName 管理员管理
 * @sign admin
 */
class AdminsController extends Controller
{
    /**
     * @Description 管理员列表
     * @Author shayvmo
     * @Date 2021/4/15 10:46 下午
     * @param AdminListRequest $adminListRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(AdminListRequest $adminListRequest)
    {
        [
            'page_size' => $page_size,
            'keyword' => $keyword,
        ] = $adminListRequest->fillData();
        $data = Admin::query()
            ->with('roles')
            ->when($keyword, function ($query, $keyword) {
                return $query
                    ->where('username', 'like', "$keyword%")
                    ->orWhere('nickname', 'like', "$keyword%");
            })->paginate($page_size)->toArray();
        $data = CommonService::changePageDataFormat($data);
        $data['data'] = collect($data['data'])->transform(function ($item, $key) {
            $item['role_names'] = collect($item['roles'])->pluck('title')->all();
            $item['roles'] = collect($item['roles'])->pluck('name')->all();
            return $item;
        });
        return $this->successData($data);
    }

    /**
     * @Description 新增管理员
     * @Author shayvmo
     * @Date 2021/4/15 10:47 下午
     * @param AdminPostRequest $adminPostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidateException
     */
    public function store(AdminPostRequest $adminPostRequest)
    {
        $params = $adminPostRequest->fillData();
        $rules = [
            'username' => [
                'required',
                'alpha_num',
                'chinese',
                'between:5,18',
                'unique:admins,username',
            ],
        ];
        CommonService::validate($params, $rules);
        $params['password'] = bcrypt(BackendConstant::DEFAULT_PASSWORD);
        $admin = Admin::query()->create($params);
        $params['roles'] && $admin->assignRole($params['roles']);
        return $this->successData($admin->toArray());
    }

    /**
     * @Description 获取单个管理员信息
     * @Author shayvmo
     * @Date 2021/4/15 10:48 下午
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Admin $admin)
    {
        $roles = $admin->getRoleNames();
        return $this->successData(array_merge($admin->toArray(), compact('roles')));
    }

    /**
     * @Description 更新管理员
     * @Author shayvmo
     * @Date 2021/4/15 10:48 下午
     * @param Admin $admin
     * @param AdminPostRequest $adminPostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ServiceException
     * @throws \Throwable
     */
    public function update(Admin $admin, AdminPostRequest $adminPostRequest)
    {
        $params = $adminPostRequest->fillData();
        if ($admin->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        if ($params['username'] !== $admin->username) {
            $rules = [
                'username' => [
                    'required',
                    'alpha_num',
                    'chinese',
                    'between:5,18',
                    'unique:admins,username',
                ],
            ];
            CommonService::validate($params, $rules);
        }
        $admin->fill($params)->saveOrFail();
        $admin->syncRoles($params['roles']);
        return $this->successData($admin->toArray());
    }

    /**
     * @Description 删除管理员
     * @Author shayvmo
     * @Date 2021/4/15 10:48 下午
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     * @throws ServiceException | \Exception
     */
    public function destroy(Admin $admin)
    {
        if($admin->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        $admin->delete();
        return $this->success();
    }

    /**
     * @Description 重置密码
     * @Author shayvmo
     * @Date 2021/4/15 10:48 下午
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     * @throws ServiceException
     */
    public function resetPassword(Admin $admin)
    {
        if($admin->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        $admin->password = bcrypt(BackendConstant::DEFAULT_PASSWORD);
        $admin->save();
        return $this->success();
    }

    /**
     * @Description 快速禁用管理员
     * @Author shayvmo
     * @Date 2021/4/15 10:51 下午
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     * @throws ServiceException
     */
    public function expressForbidden(Admin $admin)
    {
        if($admin->isSystem()) {
            throw new ServiceException(SystemConstant::CANT_EDIT_SYSTEM_ITEM);
        }
        $admin->status = !$admin->status;
        $admin->save();
        return $this->success();
    }
}
