<?php


namespace App\Http\Controllers\Backend;

use App\Constants\BackendConstant;
use App\Constants\SystemConstant;
use App\Exceptions\ServiceException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AdminLoginPost;
use App\Http\Requests\Backend\AdminUpdatePwdRequest;
use App\Models\Admin;
use App\Services\Base\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class BasicController extends Controller
{
    public function login(AdminLoginPost $adminLoginPost)
    {
        ['username' => $username, 'password' => $password] = $adminLoginPost->fillData();
        $admin = Admin::whereUsername($username)->firstOrFail();
        if (!$admin->isValid()) {
            throw new ServiceException(SystemConstant::BAN_LOGIN);
        }
        if (!Hash::check($password, $admin->password)) {
            throw new ServiceException(SystemConstant::ERROR_ACCOUNT);
        }

        // jwt登录
        $now = Carbon::now();
        $token = auth(BackendConstant::AUTH_GUARD)->claims([
            'time' => $now->timestamp,
            'endpoint' => CommonService::getRequestEndpoint(),
        ])->login($admin);

        // 登录日志
        $admin->fill([
            'last_login_ip' => Request::getClientIp(),
            'last_login_at' => $now,
        ])->save();

        return $this->successData(compact('token'));
    }

    public function logout()
    {
        Auth::logout();
        return $this->success(SystemConstant::LOGOUT_SUCCESS);
    }

    public function info()
    {
        $admin = Admin::query()->findOrFail(Auth::id());

        $permissions = $admin->getAllPermissions();

        $menus = $permissions->filter(function ($value, $key) {
            return $value->type === 1;
        })->values()->all();

        $buttons = $permissions->filter(function ($value, $key) {
            return $value->type === 2;
        });

        return $this->successData([
            'id' => $admin->id,
            'nickname' => $admin->nickname,
            'avatar' => config('app.url').$admin->avatar,
            'roles' => $admin->getRoleNames(),
            'role' => [
                'menus' => $menus,
                'buttons' => $buttons->pluck('name'),
            ],
        ]);
    }

    public function updatePassword(AdminUpdatePwdRequest $adminUpdatePwdRequest)
    {
        [
            'old_password' => $old_password,
            'password' => $password,
        ] = $adminUpdatePwdRequest->fillData();
        $admin = Admin::findOrFail(Auth::id());
        if (!Hash::check($old_password, $admin->password)) {
            throw new ServiceException(SystemConstant::WRONG_PASSWORD);
        }
        $admin->password = bcrypt($password);
        $admin->saveOrFail();
        return $this->success();
    }
}
