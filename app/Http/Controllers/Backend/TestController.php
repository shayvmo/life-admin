<?php
/**
 * laravel-backend
 *
 * @ClassName TestController
 * @Author shayvmo
 * @Date 2021-06-19 18:29 星期六
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {
//        dd($request->route()->getCompiled()->getStaticPrefix());// /backend/test
//        dd($request->method());

        dd(Carbon::now());

        /** @var Admin $admin */
        $admin = Admin::query()->find(1);

        $all_permissions = $admin->getAllPermissions();

        $all_menus = $all_permissions->filter(function ($value, $key) {
            return $value->type === 1;
        });

        $all_buttons = $all_permissions->filter(function ($value, $key) {
            return $value->type === 2;
        });

        $permission_menus = get_data_tree($all_menus->toArray());
        $permission_buttons = array_column($all_buttons->toArray(), 'name');

        $roles = $admin->getRoleNames();
        dd([$permission_menus, $permission_buttons, $roles]);
    }
}
