<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ArticleCategoryPostRequest;
use App\Http\Requests\Backend\CommonListRequest;
use App\Models\ArticleCategory;
use App\Services\Base\CommonService;

class CategoryController extends Controller
{
    public function index(CommonListRequest $commonListRequest)
    {
        [
            'page_size' => $page_size,
            'keyword' => $keyword,
        ] = $commonListRequest->fillData();
        $data = ArticleCategory::query()
            ->when($keyword, function ($query, $keyword) {
                return $query
                    ->where('name', 'like', "$keyword%");
            })->paginate($page_size)->toArray();
        $data = CommonService::changePageDataFormat($data);
        return $this->successData($data);
    }

    public function store(ArticleCategoryPostRequest $request)
    {
        $article_category = ArticleCategory::query()->create($request->fillData());
        return $this->successData($article_category->toArray());
    }

    public function show(ArticleCategory $category)
    {
        return $this->successData($category->toArray());
    }

    public function update(ArticleCategory $category, ArticleCategoryPostRequest $request)
    {
        $params = $request->fillData();
        $category->fill($params)->saveOrFail();
        return $this->successData($category->toArray());
    }

    public function destroy(ArticleCategory $category)
    {
        $category->delete();
        return $this->success();
    }

    public function expressForbidden(ArticleCategory $category)
    {
        $category->status = !$category->status;
        $category->save();
        return $this->success();
    }
}
