<?php


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Services\Base\ConfigService;
use Illuminate\Http\Request;

/**
 * Class ConfigController
 * @package App\Http\Controllers\Backend
 * @signName 配置项管理
 * @sign config
 */
class ConfigController extends Controller
{

    protected $configService;

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;

    }

    /**
     * @Description 配置项列表
     * @Author shayvmo
     * @Date 2021/4/15 10:51 下午
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = $this->configService->getAllConfig();
        return $this->successData($data);
    }

    /**
     * @Description 新增配置项
     * @Author shayvmo
     * @Date 2021/4/15 10:51 下午
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->configService->saveConfig($request->all());
        return $this->success();
    }
}
