<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTFactory;

class CheckUserLoginStatus
{
    public function handle($request, Closure $next)
    {
        $custom_claims = JWTFactory::getCustomClaims();
        if(!isset($custom_claims['time'])) {
            throw new AuthenticationException();
        }

        if (config('my.system.login_only_one_place', 0)) {
            // 校验是否多次登录
            if (!isset($custom_claims['time']) || $custom_claims['time'] !== (int)strtotime(Auth::user()->last_login_at)) {
                throw new AuthenticationException();
            }
        }

        return $next($request);
    }
}
