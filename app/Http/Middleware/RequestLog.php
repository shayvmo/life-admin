<?php

namespace App\Http\Middleware;

use Closure;

/**
 * 终端中间件 - 请求日志类
 * Class RequestLog
 * @package App\Http\Middleware
 */
class RequestLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $params = [
            'admin_id' => $request->user() ? $request->user()->id:0,
            'type_id' => 1,
            'ip_address' => $request->getClientIp(),
            'api_name' => $request->path(),
            'request_method' => $request->method(),
            'request_header' => json_encode($request->header(), JSON_UNESCAPED_UNICODE),
            'request_get' => json_encode($request->query(), JSON_UNESCAPED_UNICODE),
            'request_post' => json_encode($request->post(), JSON_UNESCAPED_UNICODE),
            'response_content' => $response->getContent(),
            'remark' => '',
        ];
        \App\Services\Base\CommonService::addRequestLog($params);
    }
}
