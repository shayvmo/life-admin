<?php
/**
 * eshop
 *
 * @ClassName RefreshJwtToken
 * @Author Eric
 * @Date 2021-08-01 23:53 星期日
 * @Version 1.0
 * @Description
 */


namespace App\Http\Middleware;


use App\Services\Base\CommonService;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class RefreshJwtToken extends BaseMiddleware
{
    public function handle($request, \Closure $next, $guard)
    {
        $this->checkForToken($request);

        try {

            $now_endpoint = CommonService::getRequestEndpoint();
            $pay_load = $this->auth->parseToken()->getPayload()->toArray();
            // 插件没有namespace 会造成问题
            if (!isset($pay_load['endpoint'], $pay_load['time'])
                || ($now_endpoint && $now_endpoint !== $pay_load['endpoint'])
            ) {
                throw new AuthenticationException();
            }

            if (auth($guard)->check()) {

                return $next($request);
            }
            throw new UnauthorizedHttpException('jwt-auth', '未登录');

        } catch (TokenExpiredException $exception) {

            try {

                $token = $this->auth->refresh();

                $this->auth->setToken($token);

                $request->headers->set('Authorization','Bearer '.$token);

            } catch (JWTException $exception) {

                throw new UnauthorizedHttpException('jwt-auth', $exception->getMessage());

            }

        } catch (JWTException $e) {

            throw new UnauthorizedHttpException('jwt-auth', '未登录');

        }

        return $this->setAuthenticationHeader($next($request), $token);
    }

    protected function setAuthenticationHeader($response, $token = null)
    {
        $token = $token ?: $this->auth->refresh();
        $response->headers->set('Authorization', $token);
        $response->headers->set('Access-Control-Expose-Headers', 'Authorization');
        return $response;
    }
}
