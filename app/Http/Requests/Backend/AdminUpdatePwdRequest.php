<?php


namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

/**
 * 修改密码请求
 * Class AdminUpdatePwdRequest
 * @package App\Http\Requests\Backend
 */
class AdminUpdatePwdRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'old_password' => [
                'required',
                'alpha_num',
                'chinese',
                'between:6,18',

            ],
            'password' => [
                'required',
                'confirmed',
                'alpha_num',
                'chinese',
                'between:6,18',
            ],
            'password_confirmation' => [
                'required',
                'alpha_num',
                'chinese',
                'between:6,18',
            ],
        ];
    }

    public function fillData()
    {
        return [
            'old_password' => $this->post('old_password'),
            'password' => $this->post('password'),
            'password_confirmation' => $this->post('password_confirmation'),
        ];
    }
}
