<?php
/**
 * life-admin
 *
 * @ClassName ArticleCategoryPostRequest
 * @Author Eric
 * @Date 2021-11-14 17:52 星期日
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend;


use App\Http\Requests\BaseRequest;

class ArticleCategoryPostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name'=>[
                'required',
                'string',
                'max:20',
            ],
            'status'=>[
                'required',
                'integer',
                'in:0,1'
            ],
        ];
    }

    public function fillData()
    {
        return [
            'name' => $this->input('name'),
            'status' => $this->input('status') ?: 0,
        ];
    }
}
