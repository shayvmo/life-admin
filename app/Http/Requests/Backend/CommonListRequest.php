<?php
/**
 * life-admin
 *
 * @ClassName CommonListRequest
 * @Author Eric
 * @Date 2021-11-14 17:46 星期日
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend;


use App\Http\Requests\PagePost;

class CommonListRequest extends PagePost
{
    public function rules()
    {
        return array_merge(parent::rules(),[
            'keyword'=>[
                'sometimes',
                'string',
                'max:30',
                'nullable',
            ],
        ]);
    }

    public function fillData()
    {
        return array_merge(parent::fillData(), ['keyword' => $this->get('keyword')]);
    }
}
