<?php


namespace App\Http\Requests\Backend;

use App\Http\Requests\PagePost;

class MenuListRequest extends PagePost
{
    public function rules()
    {
        return array_merge(parent::rules(),[
            'pid'=>[
                'sometimes',
                'integer',
                'between:0,10000',
                'nullable',
            ],
            'name'=>[
                'sometimes',
                'string',
                'between:2,20',
                'nullable',
            ],
        ]);
    }

    public function fillData()
    {
        return [
            'page' => $this->get('page'),
            'page_size' => $this->get('page_size'),
            'pid' => $this->get('pid'),
            'name' => $this->get('name'),
        ];
    }
}
