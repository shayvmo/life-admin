<?php

namespace App\Models;


/**
 * @property int $id
 * @property string $group  类型： eg system 系统 common 公共 order 订单
 * @property string $sign  配置项key, 主要用于前端
 * @property string $config_sign  配置项key, 唯一值
 * @property string $name  配置项名称
 * @property string $value  配置项value
 * @property string $field_type  字段类型
 * @property string $option_value  可选值
 * @property string $remark  备注
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 */
class Config extends BaseModel
{
    protected $table = 'configs';

    /**
     * @var array
     */
    protected $fillable = [
        'group',
        'sign',
        'config_sign',
        'name',
        'value',
        'field_type',
        'option_value',
        'remark'
    ];

    protected $casts = [
        'option_value' => 'array',
    ];
}
