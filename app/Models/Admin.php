<?php

namespace App\Models;

use App\Constants\BackendConstant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int $id
 * @property string $username  登录账号
 * @property string $password  密码
 * @property string $nickname  呢称
 * @property string $avatar  头像
 * @property string $email  邮箱
 * @property int $system_status  系统保留 1 是 0 否
 * @property int $status  1启用 0停用
 * @property string $mobile  手机号
 * @property string $last_login_ip  上次登录IP
 * @property string $last_login_at  上次登录时间
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 */
class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes, HasRoles;

    protected $table = 'admins';

    protected $attributes = [
        'avatar' => BackendConstant::DEFAULT_AVATAR
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'nickname',
        'avatar',
        'email',
        'system_status',
        'status',
        'mobile',
        'last_login_ip',
        'last_login_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dispatchesEvents = [
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * 返回一个键值数组，其中包含要添加到JWT的任何自定义声明。
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isValid(): bool
    {
        return (int) $this->status === 1;
    }

    public function isSystem(): bool
    {
        return (int) $this->system_status === 1;
    }

    public function getCreatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

}
