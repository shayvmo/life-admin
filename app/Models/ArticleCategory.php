<?php


namespace App\Models;


class ArticleCategory extends BaseModel
{
    protected $table = 'article_categories';

    protected $fillable = [
        'name',
        'status',
    ];
}
