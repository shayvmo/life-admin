<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property   int  $id
 * @property   int  $admin_id  管理员ID
 * @property   int  $type_id  类型 1日志 2错误 3警告
 * @property   string  $ip_address  IP地址
 * @property   string  $api_name  接口名称
 * @property   string  $request_method  请求方式
 * @property   string  $request_header  请求头
 * @property   string  $request_get  请求内容get
 * @property   string  $request_post  请求内容post
 * @property   string  $response_content  响应内容
 * @property   string  $remark  备注
 * @property   \Carbon\Carbon  $created_at
 * @property   \Carbon\Carbon  $updated_at
 */
class RequestLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'admin_id',
        'type_id',
        'ip_address',
        'api_name',
        'request_method',
        'request_header',
        'request_get',
        'request_post',
        'response_content',
        'remark'
    ];

}
