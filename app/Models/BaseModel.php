<?php

namespace App\Models;

use App\Constants\SystemConstant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

abstract class BaseModel extends Model
{
    use SoftDeletes;

    public const SHOW_STATUS = 1;// status = 1 启用

    public const NOT_USE_STATUS = 0;// status = 0 停用

    public function getPerPage()
    {
        return SystemConstant::DEFAULT_PAGE_SIZE;
    }

    public function getCreatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }


}
