<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * eshop
 *
 * @ClassName User
 * @Author Eric
 * @Date 2021-07-31 10:15 星期六
 * @Version 1.0
 * @Description
 * @package App\Models
 *
 * @property int $id
 * @property string $openid  openid
 * @property string $password  密码
 * @property string $avatar  头像
 * @property string $nickname  呢称
 * @property string $mobile  手机号
 * @property int $status  状态1 正常 0 禁用
 * @property string $api_token  状态1 正常 0 禁用
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 *
 */
class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'openid',
        'password',
        'avatar',
        'nickname',
        'mobile',
        'status',
        'api_token',
    ];

    public function generateToken()
    {
        $token = Str::random(128);
        $api_token = hash('sha256', $token);
        $this->api_token = $api_token;
        $this->save();
        return $token;
    }

    public function getCreatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

}
