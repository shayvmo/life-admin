<?php


namespace App\Constants;


class BackendConstant
{
    // 守卫
    public const AUTH_GUARD = 'backend';

    // 默认密码
    public const DEFAULT_PASSWORD = '123456';

    // 默认头像
    public const DEFAULT_AVATAR = '/source/sample.jpg';
}
