### 开心品生活-后端开源

本项目是基于` uniapp `模板 [https://ext.dcloud.net.cn/plugin?id=1988](https://ext.dcloud.net.cn/plugin?id=1988) 开源的后端程序

原模板基于` MIT `协议，因此本仓库也遵守 `MIT` 协议。

#### 安装教程

【配置伪静态】
```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

1、安装依赖的composer包
```shell script
composer install
```

2、访问` http://域名/install `，即可设置数据库地址。

设置完成后，运行上述2个步骤即可。

【注】

1、本地开发部署时，建议直接使用 life.test 作为域名，安装成功后，可以直接登录访问后台，默认 admin 123456

2、清除安装锁命令: ` php artisan install:clean `

#### 一、项目架构

Laravel8 + vue-element-admin 

本仓库地址：[https://gitee.com/shayvmo/life-admin](https://gitee.com/shayvmo/life-admin)

后端 vue 仓库：[https://gitee.com/shayvmo/life-vue-admin](https://gitee.com/shayvmo/life-vue-admin)

#### 二、Git 提交说明

[!] 修改BUG

[+] 增加功能

[*] 普通修改

[-] 减少功能

#### 三、参与开源

1、fork 仓库

2、推送代码到 develop 分支，提交Pull Request


#### 四、项目交流

QQ：1006897172

或「 开心品生活 」原作者QQ交流群：1054938991
