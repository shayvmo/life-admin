<?php

namespace Addons\QuickStart\Http\Controllers;

use Illuminate\Filesystem\Filesystem;

class IndexController
{
    public function index()
    {
//        $file = app()->make(Filesystem::class);
//        $dirs = $file->directories(base_path('addons'));
//        $signs = [];
//        foreach ($dirs as $dir) {
//            $signs[] = $file->name($dir);
//        }
//        dd($signs);

        return view('QuickStart::index');
    }
}
