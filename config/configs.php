<?php

return [
    [
        'group' => 'app',
        'sign' => 'app_name',
        'config_sign' => 'app.name',
        'name' => '应用名称',
        'value' => 'shayvmo',
        'field_type' => 'text',
        'remark' => '应用名称',
    ],
    [
        'group' => 'app',
        'sign' => 'app_url',
        'config_sign' => 'app.url',
        'name' => '网站地址',
        'value' => '',
        'field_type' => 'text',
        'remark' => '网站地址',
    ],
    [
        'group' => 'my',
        'sign' => 'my_system_system_open_tag',
        'config_sign' => 'my.system.system_open_tag',
        'name' => '系统开放标识',
        'value' => 1,
        'field_type' => 'switch',
        'remark' => '系统开放标识： 1 开放 0 未开放',
    ],
    [
        'group' => 'my',
        'sign' => 'my_system_login_only_one_place',
        'config_sign' => 'my.system.login_only_one_place',
        'name' => '后台系统登录互挤',
        'value' => 0,
        'field_type' => 'switch',
        'remark' => '后台系统登录互挤: 1 互挤 0 否',
    ],
    [
        'group' => 'upload',
        'sign' => 'upload_file_size',
        'config_sign' => 'my.upload.file_size',
        'name' => '文件最大值(MB)',
        'value' => 10,
        'field_type' => 'text',
        'remark' => '默认文件最大10M，单位：MB',
    ],
];
