<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'configs';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('group',20)->nullable(false)->comment('类型： eg system 系统 common 公共 order 订单');
            $table->string('sign',50)->unique()->nullable(false)->comment('配置项key, 主要用于前端');
            $table->string('config_sign',50)->unique()->nullable(false)->comment('配置项key, 唯一值');
            $table->string('name',20)->nullable()->comment('配置项名称');
            $table->string('value',2000)->nullable()->comment('配置项value');
            $table->string('field_type', 20)->default('')->comment('字段类型');
            $table->string('option_value', 2000)->nullable()->default(null)->comment('可选值');
            $table->string('remark')->nullable()->comment('备注');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '配置表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
