<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 15)->unique()->comment('登录账号');
            $table->string('password')->comment('密码');
            $table->string('nickname', 50)->comment('呢称');
            $table->string('avatar', 200)->nullable()->default('')->comment('头像');
            $table->string('email', 50)->nullable()->default('')->comment('邮箱');
            $table->tinyInteger('system_status')->default(0)->comment('系统保留 1 是 0 否');
            $table->tinyInteger('status')->default(1)->comment('1启用 0停用');
            $table->string('last_login_ip', 32)->default('')->comment('上次登录IP');
            $table->timestamp('last_login_at')->nullable()->comment('上次登录时间');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
