<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedConfig extends Migration
{
    private $configs;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->configs = config('configs');

        if ($this->configs) {

            $model = new \App\Models\Config();
            foreach ($this->configs as $config) {
                $temp = clone $model;
                $temp->create($config);
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->configs = config('configs');

        if ($this->configs) {

            $configs_collection = collect($this->configs);

            \App\Models\Config::query()->whereIn('sign', $configs_collection->pluck('sign'))->delete();

        }
    }
}
