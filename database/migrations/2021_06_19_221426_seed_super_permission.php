<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedSuperPermission extends Migration
{
    private $permissions;

    private $permissions_arr;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->permissions = config('permissions', []);

        if ($this->permissions) {
            /** @var \App\Models\Role $role */
            $role = \App\Models\Role::create([
                'name' => 'super-admin',
                'guard_name' => \App\Constants\BackendConstant::AUTH_GUARD,
                'title' => '超级管理员',
                'desc' => '',
                'system_status' => 1,
            ]);

            $admin = \App\Models\Admin::create([
                'username' => 'admin',
                'password' => bcrypt('123456'),
                'nickname' => '超级管理员',
                'avatar' => '/storage/sample.jpg',
                'system_status' => 1,
            ]);

            $this->savePermission($this->permissions);


            $admin->assignRole($role);

            $this->permissions_arr && $role->givePermissionTo($this->permissions_arr);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->permissions = config('permissions', []);

        if ($this->permissions) {
            \App\Models\Permission::query()
                ->where('guard_name', \App\Constants\BackendConstant::AUTH_GUARD)
                ->whereIn('name', array_column($this->permissions, 'name'))
                ->delete();

            \App\Models\Role::query()
                ->where('name', 'super-admin')
                ->where('guard_name', \App\Constants\BackendConstant::AUTH_GUARD)
                ->delete();
        }
    }

    private function savePermission(array $permissions, int $pid = 0)
    {
        foreach ($permissions as $item) {
            $temp = [
                'pid' => $pid,
                'name' => $item['name'],
                'guard_name' => \App\Constants\BackendConstant::AUTH_GUARD,
                'sign' => $item['sign'],
                'title' => $item['title'],
                'icon' => $item['icon'],
                'type' => $item['type'],
                'sort' => $item['sort'],
                'route' => $item['route'],
                'method' => $item['method'],
            ];
            $permission = \App\Models\Permission::create($temp);
            $permission->path = ($permission->pid > 0 ? $permission->parent->path : '0').'-'.$permission->id;
            $permission->save();
            $this->permissions_arr[] = $item['name'];
            if (!empty($item['children'])) {
                $this->savePermission($item['children'], $permission->id);
            }
        }

    }
}
