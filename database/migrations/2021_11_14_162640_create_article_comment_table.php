<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('article_id')->nullable(false)->default(0)->comment('文章ID');
            $table->unsignedInteger('user_id')->nullable(false)->default(0)->comment('用户ID');
            $table->unsignedInteger('pid')->nullable(false)->default(0)->comment('回复评论ID');
            $table->string('content')->nullable(false)->default('')->comment('回复内容');
            $table->unsignedInteger('reply_count')->nullable(false)->default(0)->comment('回复统计');
            $table->unsignedInteger('like_count')->nullable(false)->default(0)->comment('点赞统计');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_comments');
    }
}
