<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'users';
        Schema::create($table_name, function (Blueprint $table) {
            $table->id();
            $table->string('openid', 100)->unique()->comment('openid');
            $table->string('password')->comment('密码');
            $table->string('avatar')->default('')->comment('头像');
            $table->string('nickname', 50)->default('')->comment('呢称');
            $table->string('mobile', 15)->default('')->comment('手机号');
            $table->unsignedTinyInteger('status')->default(1)->comment('状态1 正常 0 禁用');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '用户表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
