<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->default(0)->comment('分类ID');
            $table->string('title', 100)->nullable(false)->default('')->comment('文章标题');
            $table->unsignedInteger('read')->nullable(false)->default(0)->comment('阅读量');
            $table->string('source', 50)->nullable(false)->default('')->comment('作者');
            $table->string('source_url')->nullable(false)->default('')->comment('原文链接');
            $table->string('photo_url')->nullable(false)->default('')->comment('首图地址');
            $table->text('content')->nullable(false)->comment('文章内容');
            $table->unsignedInteger('like_count')->default(0)->comment('点赞数');
            $table->unsignedInteger('favorite_count')->default(0)->comment('收藏数');
            $table->unsignedTinyInteger('status')->default(0)->comment('1 显示 0 隐藏');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
