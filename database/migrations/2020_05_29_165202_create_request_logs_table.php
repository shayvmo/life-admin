<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 请求日志
 * Class CreateRequestLogsTable
 */
class CreateRequestLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'request_logs';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->nullable()->comment('管理员ID');
            $table->tinyInteger('type_id')->default(1)->comment('类型 1日志 2错误 3警告');
            $table->string('ip_address',20)->nullable(false)->comment('IP地址');
            $table->string('api_name',100)->nullable(false)->comment('接口名称');
            $table->string('request_method',20)->nullable(false)->comment('请求方式');
            $table->text('request_header')->nullable(false)->comment('请求头');
            $table->text('request_get')->nullable(false)->comment('请求内容get');
            $table->text('request_post')->nullable(false)->comment('请求内容post');
            $table->longText('response_content')->nullable(false)->comment('响应内容');
            $table->string('remark')->nullable()->comment('备注');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '请求日志表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_logs');
    }
}
