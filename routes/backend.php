<?php

Route::group(['namespace' => 'Backend', 'middleware' => 'request.log', ], function () {
//
    Route::any('test', 'TestController@test');//调试接口

    Route::post('login', 'BasicController@login');//登录

    // 必须登录后请求
    Route::group(['middleware' => ['jwt.token.refresh:backend', 'auth:backend', 'check.user.login.status']], function () {

        Route::get('info', 'BasicController@info');//当前登录用户信息

        Route::put('updatePassword', 'BasicController@updatePassword');//更新用户密码

        Route::post('logout', 'BasicController@logout');//退出登录

        // 权限
        Route::group(['middleware' => 'check.user.auth'], function(){
            Route::apiResource('admin', 'AdminsController');//管理员相关
            Route::group(['prefix' => 'admin'], function () {
                Route::put('forbidden/{admin}', 'AdminsController@expressForbidden');//快速禁用
                Route::put('resetPassword/{admin}', 'AdminsController@resetPassword');//重置密码
            });

            Route::apiResource('role', 'RolesController');//角色相关
            Route::group(['prefix' => 'role'], function () {
                Route::get('permission/{role}', 'RolesController@getRolePermissions');//获取角色权限
                Route::put('permission/{role}', 'RolesController@setRolePermissions');//设置角色权限
            });

            Route::apiResource('permission', 'PermissionController');// 统一权限相关

            Route::apiResource('config', 'ConfigController')->only([
                'index','store'
            ]);//配置项

            Route::apiResource('category', 'CategoryController');//文章分类相关
            Route::group(['prefix' => 'category'], function () {
                Route::put('forbidden/{category}', 'CategoryController@expressForbidden');//快速禁用
            });
        });
    });

});
