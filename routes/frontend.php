<?php


Route::group(['namespace' => 'Frontend','middleware'=>'request.log'], function () {

    Route::group([
        'namespace' => 'v1',
        'prefix' => 'v1',
    ], function () {

        Route::get('demo', 'DemoController@index');//调试接口
        Route::post('demo-login', 'DemoController@login');//调试接口

        Route::group(['middleware'=>'auth:api'], function () {
            Route::get('demo-after-login', 'DemoController@test');//调试接口
        });

    });
});
