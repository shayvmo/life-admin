<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

// 安装程序
Route::get('install', '\App\Http\Controllers\Install\IndexController@index');
Route::post('install/save', '\App\Http\Controllers\Install\IndexController@save');
Route::post('install/execute', '\App\Http\Controllers\Install\IndexController@execute');

